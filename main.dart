import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:module_5/usersList.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: FirebaseOptions(
        apiKey: "AIzaSyABmVE-f63m6yq9BzCiCANws1G_rbDUcJI",
        authDomain: "mtnmodule5-ee0ab.firebaseapp.com",
        projectId: "mtnmodule5-ee0ab",
        storageBucket: "mtnmodule5-ee0ab.appspot.com",
        messagingSenderId: "687657327131",
        appId: "1:687657327131:web:4c898b50d2d7c17f47b6be"),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Module_5'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AddUser(),
          ],
        ),
      ),
    );
  }
}

class AddUser extends StatefulWidget {
  const AddUser({
    Key? key,
  }) : super(key: key);

  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  final registerFormKey = GlobalKey<FormState>();

  late TextEditingController nameController;
  late TextEditingController surnameController;
  late TextEditingController cityController;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController();
    surnameController = TextEditingController();
    cityController = TextEditingController();
  }

  @override
  void dispose() {
    nameController.dispose();
    surnameController.dispose();
    cityController.dispose();
    super.dispose();
  }

  Future _create() {
    final name = nameController.text;
    final surname = surnameController.text;
    final city = cityController.text;

    final ref = FirebaseFirestore.instance.collection("users").doc();
    return ref
        .set({"Name": name, "Surname": surname, "City": city})
        .then((value) => print("User added"))
        .onError((error, stackTrace) => print(error.toString()));
  }

  

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Form(
            key: registerFormKey,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    'Register a new User',
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    validator: ((value) {
                      if (value == null || value.isEmpty) {
                        return "Please enter your name";
                      } else {
                        return null;
                      }
                    }),
                    controller: nameController,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    validator: ((value) {
                      if (value == null || value.isEmpty) {
                        return "Please enter your surname";
                      } else {
                        return null;
                      }
                    }),
                    controller: surnameController,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    validator: ((value) {
                      if (value == null || value.isEmpty) {
                        return "Please enter your city";
                      } else {
                        return null;
                      }
                    }),
                    controller: cityController,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: const Size.fromHeight(40),
                    ),
                    onPressed: () {
                      if (registerFormKey.currentState?.validate() ?? false) {
                        _create();
                      }
                    },
                    child: const Text('Register'),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: const Size.fromHeight(40),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const UsersList(),
                        ),
                      );
                    },
                    child: const Text('users'),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: const Size.fromHeight(40),
                    ),
                    onPressed: () {
                      final ref =
                          FirebaseFirestore.instance.collection("users").doc();
                      //final ref2 = ref.id;

                      ref.update({
                        'Name': nameController.text,
                        'Surname': surnameController.text,
                        'City': cityController.text,
                      });
                    },
                    child: const Text('update'),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
